package com.arisgeorgoulis.bakingapp.APIClients;

import android.util.Log;

import com.arisgeorgoulis.bakingapp.API.RecipeAPI;
import com.arisgeorgoulis.bakingapp.data.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecipeAPIClient implements Callback<List<Result>> {
    private updateTheAdapterListener<List<Result>> listener;

    public RecipeAPIClient(updateTheAdapterListener listener){
        this.listener = listener;
    }

    public void getTheRecipes() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RecipeAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create the service
        RecipeAPI service = retrofit.create(RecipeAPI.class);

        Call<List<Result>> call = service.getRecipes();

        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Result>> call, Response<List<Result>> response) {
        List<Result> recipes = response.body();
        listener.updateTheAdapter(recipes);
    }

    @Override
    public void onFailure(Call<List<Result>> call, Throwable t) {
        Log.e("Response has failed :", t.toString());
    }

    public interface updateTheAdapterListener<T> {
        void updateTheAdapter(T t);
    }
}
