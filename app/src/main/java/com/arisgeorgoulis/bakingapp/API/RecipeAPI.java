package com.arisgeorgoulis.bakingapp.API;

import com.arisgeorgoulis.bakingapp.data.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RecipeAPI {
    static final String BASE_URL = "https://d17h27t6h515a5.cloudfront.net";

    @GET("/topher/2017/May/59121517_baking/baking.json")
    Call<List<Result>> getRecipes();
}
