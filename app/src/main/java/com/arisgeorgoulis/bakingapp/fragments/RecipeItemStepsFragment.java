package com.arisgeorgoulis.bakingapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arisgeorgoulis.bakingapp.R;
import com.arisgeorgoulis.bakingapp.adapters.RecipeItemStepsAdapter;
import com.arisgeorgoulis.bakingapp.data.Step;
import com.arisgeorgoulis.bakingapp.ui.DetailedSteps;
import com.arisgeorgoulis.bakingapp.ui.RecipeItem;
import com.arisgeorgoulis.bakingapp.utilities.TabletUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeItemStepsFragment extends Fragment implements RecipeItemStepsAdapter.RecipeStepsOnClickHandler{

    // Parcelable constants
    private final static String PARC_STEPS = "StepsParcelable";

    @BindView(R.id.recipe_steps)
    RecyclerView recipeSteps;
    private RecipeItemStepsAdapter recipeListAdapter;
    private RecyclerView.LayoutManager recipeStepsLayoutManager;

    private List<Step> steps;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_recipe_steps, container, false);

        // Bind the fields
        ButterKnife.bind(this, rootView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recipeSteps.setHasFixedSize(false);

        recipeStepsLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recipeSteps.setLayoutManager(recipeStepsLayoutManager);

        if (savedInstanceState != null) {
            this.steps = savedInstanceState.getParcelableArrayList(PARC_STEPS);
        }

        recipeListAdapter = new RecipeItemStepsAdapter(this.steps, this);
        recipeSteps.setAdapter(recipeListAdapter);

        return rootView;
    }

    // Setter
    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(PARC_STEPS, (ArrayList<? extends Parcelable>) this.steps);
    }

    @Override
    public void recipeStepsOnClick(Step step) {
        if (TabletUtils.IS_TABLET == false) {
            Intent intent = new Intent(getContext(), DetailedSteps.class);
            intent.putExtra(RecipeItem.STEP, step);
            startActivity(intent);
        } else {
            RecipeDetailFragment detailFragment = new RecipeDetailFragment();
            detailFragment.setStep(step);

            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.recipe_item_details, detailFragment)
                    .commit();
        }
    }
}
