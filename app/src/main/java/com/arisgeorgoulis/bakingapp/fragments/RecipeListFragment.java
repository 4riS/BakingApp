package com.arisgeorgoulis.bakingapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arisgeorgoulis.bakingapp.APIClients.RecipeAPIClient;
import com.arisgeorgoulis.bakingapp.MainActivity;
import com.arisgeorgoulis.bakingapp.R;
import com.arisgeorgoulis.bakingapp.adapters.RecipeListAdapter;
import com.arisgeorgoulis.bakingapp.data.Ingredient;
import com.arisgeorgoulis.bakingapp.data.Result;
import com.arisgeorgoulis.bakingapp.data.Step;
import com.arisgeorgoulis.bakingapp.ui.RecipeItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeListFragment extends Fragment implements RecipeListAdapter.RecipeAdapterOnClickHandler, RecipeAPIClient.updateTheAdapterListener<List<Result>>{

    // Parcelable constants
    private final static String PARC_RECIPES = "RecipesParcelable";
    private final static String PARC_LIST_POSITION = "RecipesPositionOnList";


    @BindView(R.id.recipe_list)
    RecyclerView recipeList;
    private RecipeListAdapter recipeListAdapter;
    private LinearLayoutManager recipeListLayoutManager;
    private List<Result> recipes = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_recipe_list, container, false);

        // Bind the fields
        ButterKnife.bind(this, rootView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recipeList.setHasFixedSize(false);

        // Create the layout for recipe list
        recipeListLayoutManager = new LinearLayoutManager(getContext());
        recipeList.setLayoutManager(recipeListLayoutManager);

        if (savedInstanceState != null) {
            this.recipes = savedInstanceState.getParcelableArrayList(PARC_RECIPES);
        }

        // Create the adapter
        recipeListAdapter = new RecipeListAdapter(this.recipes, this);

        // Set the adapter on the GridView
        recipeList.setAdapter(recipeListAdapter);

        if (savedInstanceState != null) {
            int mCurrentPosition = savedInstanceState.getInt(PARC_LIST_POSITION);
            if (mCurrentPosition != -1){
                recipeList.smoothScrollToPosition(mCurrentPosition);
            }
        }

        // Return the root view
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            RecipeAPIClient recipeAPICLient = new RecipeAPIClient(this);
            recipeAPICLient.getTheRecipes();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(PARC_RECIPES, (ArrayList<? extends Parcelable>) this.recipes);
        outState.putInt(PARC_LIST_POSITION, recipeListLayoutManager.findFirstVisibleItemPosition());
    }

    @Override
    public void updateTheAdapter(List<Result> recipes) {
        this.recipes = recipes;
        recipeListAdapter.recipesHaveUpdated(this.recipes);
    }

    @Override
    public void recipeListOnClick(Result recipe) {
        List<Ingredient> ingredients;
        ingredients = recipe.getIngredients();

        List<Step> steps;
        steps = recipe.getSteps();

        Intent intent = new Intent(getActivity(), RecipeItem.class);
        intent.putParcelableArrayListExtra(MainActivity.RECIPE_INGREDIENTS, (ArrayList<? extends Parcelable>) ingredients);
        intent.putParcelableArrayListExtra(MainActivity.RECIPE_STEPS, (ArrayList<? extends Parcelable>) steps);
        startActivity(intent);
    }
}
