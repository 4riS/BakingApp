package com.arisgeorgoulis.bakingapp.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arisgeorgoulis.bakingapp.R;
import com.arisgeorgoulis.bakingapp.data.Step;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeDetailFragment extends Fragment {
    // Parcelable constants
    private final static String PARC_DETAIL = "DetailParcelable";
    private final static String PARC_PLAYBACK_POS = "PlaybackPosParcelable";
    private final static String PARC_PLAY_WHEN_READY = "PlayWhenReadyParcelable";

    Step step;

    @BindView(R.id.video_player)
    PlayerView videoView;
    @BindView(R.id.recipe_details_heading)
    TextView detailHeading;
    @BindView(R.id.recipe_details_description)
    TextView detailDescription;
    @BindView(R.id.no_image)
    ImageView thumbnail;

    String videoURL;
    long playbackPos;
    boolean playWhenReady;
    SimpleExoPlayer videoPlayer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_recipe_details, container, false);

        // Bind the fields
        ButterKnife.bind(this, rootView);

        if (savedInstanceState != null) {
            this.step = savedInstanceState.getParcelable(PARC_DETAIL);
            this.playbackPos = savedInstanceState.getLong(PARC_PLAYBACK_POS);
            this.playWhenReady = savedInstanceState.getBoolean(PARC_PLAY_WHEN_READY);
        } else {
            this.playbackPos = 0;
            this.playWhenReady = true;
        }

        videoURL = this.step.getVideoURL();
        detailHeading.setText(step.getShortDescription());
        detailDescription.setText(step.getDescription());

        if (TextUtils.isEmpty(videoURL)){
            videoView.setVisibility(View.GONE);
            if (TextUtils.isEmpty(step.getThumbnailURL())){
                thumbnail.setVisibility((View.GONE));
            } else {
                Picasso.get().load(step.getThumbnailURL()).into(thumbnail);
            }
        } else {
            thumbnail.setVisibility(View.GONE);
            initializePlayer(Uri.parse(videoURL));
        }

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer(Uri.parse(videoURL));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Util.SDK_INT <= 23 || videoPlayer == null) {
            initializePlayer(Uri.parse(videoURL));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    //Initialize ExoPlayer.
    private void initializePlayer(Uri mediaUri) {
        if (videoPlayer == null) {

            // Create an instance of the ExoPlayer.
            videoPlayer = ExoPlayerFactory.newSimpleInstance(
                    new DefaultRenderersFactory(getContext()),
                    new DefaultTrackSelector(), new DefaultLoadControl());
            videoView.setPlayer(videoPlayer);

            // Prepare the MediaSource.
            MediaSource mediaSource = buildMediaSource(mediaUri);
            videoPlayer.prepare(mediaSource);
            videoPlayer.setPlayWhenReady(playWhenReady);
            videoPlayer.seekTo(playbackPos);
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultHttpDataSourceFactory(getString(R.string.app_name))).
                createMediaSource(uri);
    }


    private void releasePlayer() {
        if (videoPlayer != null) {
            playbackPos = videoPlayer.getContentPosition();
            playWhenReady = videoPlayer.getPlayWhenReady();
            videoPlayer.release();
            videoPlayer = null;
        }
    }

    public void setStep(Step step) {
        this.step = step;
    }

    private void saveState() {
        if (videoPlayer != null) {
            playbackPos = videoPlayer.getContentPosition();
            playWhenReady = videoPlayer.getPlayWhenReady();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        saveState();
        outState.putParcelable(PARC_DETAIL, step);
        outState.putLong(PARC_PLAYBACK_POS, this.playbackPos);
        outState.putBoolean(PARC_PLAY_WHEN_READY, this.playWhenReady);
    }
}
