package com.arisgeorgoulis.bakingapp.fragments;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arisgeorgoulis.bakingapp.R;
import com.arisgeorgoulis.bakingapp.adapters.RecipeItemIngredientsAdapter;
import com.arisgeorgoulis.bakingapp.data.Ingredient;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeItemIngredientsFragment extends Fragment {

    // Parcelable constants
    private final static String PARC_INGREDIENTS = "IngredientsParcelable";

    @BindView(R.id.recipe_ingredients)
    RecyclerView recipeIngredients;
    private RecipeItemIngredientsAdapter recipeListAdapter;
    private RecyclerView.LayoutManager recipeIngredientsLayoutManager;

    private List<Ingredient> ingredients = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_recipe_ingredients, container, false);

        // Bind the fields
        ButterKnife.bind(this, rootView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recipeIngredients.setHasFixedSize(false);

        recipeIngredientsLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recipeIngredients.setLayoutManager(recipeIngredientsLayoutManager);

        if (savedInstanceState != null) {
            this.ingredients = savedInstanceState.getParcelableArrayList(PARC_INGREDIENTS);
        }

        recipeListAdapter = new RecipeItemIngredientsAdapter(this.ingredients);
        recipeIngredients.setAdapter(recipeListAdapter);

        return rootView;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(PARC_INGREDIENTS, (ArrayList<? extends Parcelable>) ingredients);
    }
}
