package com.arisgeorgoulis.bakingapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arisgeorgoulis.bakingapp.R;
import com.arisgeorgoulis.bakingapp.data.Step;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeItemStepsAdapter extends RecyclerView.Adapter<RecipeItemStepsAdapter.RecipeItemStepsAdapterViewHolder>{

    private List<Step> steps;
    private RecipeStepsOnClickHandler listener;

    public interface RecipeStepsOnClickHandler{
        public void recipeStepsOnClick(Step step);
    }

    public RecipeItemStepsAdapter(List<Step> steps, RecipeStepsOnClickHandler listener){
        this.steps = steps;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecipeItemStepsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForRecyclerItem = R.layout.recipe_item_steps;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;
        View view = inflater.inflate(layoutIdForRecyclerItem, parent, shouldAttachToParentImmediately);
        return new RecipeItemStepsAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeItemStepsAdapterViewHolder holder, int position) {
        Step step = steps.get(position);
        holder.recipeStep.setText(String.valueOf(step.getId()) + "." + step.getShortDescription());
    }

    @Override
    public int getItemCount() {
        if (steps == null) {
            return 0;
        } else {
            return steps.size();
        }
    }

    public class RecipeItemStepsAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.recipe_steps_placeholder)
        TextView recipeStep;

        public RecipeItemStepsAdapterViewHolder(View itemView) {
            super(itemView);

            // Bind the fields
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            listener.recipeStepsOnClick(steps.get(clickedPosition));
        }
    }
}
