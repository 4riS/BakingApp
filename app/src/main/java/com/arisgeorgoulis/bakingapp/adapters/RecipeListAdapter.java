package com.arisgeorgoulis.bakingapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arisgeorgoulis.bakingapp.R;
import com.arisgeorgoulis.bakingapp.data.Result;
import com.arisgeorgoulis.bakingapp.utilities.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.RecipeListAdapterViewHolder>{
    private List<Result> recipes;
    private RecipeAdapterOnClickHandler listener;

    public interface RecipeAdapterOnClickHandler {
        public void recipeListOnClick(Result recipe);
    }

    public RecipeListAdapter(List<Result> recipes, RecipeAdapterOnClickHandler listener) {
        this.recipes = recipes;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecipeListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForRecyclerItem = R.layout.recipe_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;
        View view = inflater.inflate(layoutIdForRecyclerItem, parent, shouldAttachToParentImmediately);
        return new RecipeListAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeListAdapterViewHolder holder, int position) {
        Result recipe = recipes.get(position);
        holder.recipeListItemName.setText(recipe.getName());
        holder.recipeListItemServingPlaceholder.setText(String.valueOf(recipe.getServings()));
        if (recipe.getImage().equals("")){
            holder.recipeImage.setVisibility(View.GONE);
        } else {
            if (NetworkUtils.isOnline(holder.recipeImage.getContext())){
                Picasso.get().load(recipe.getImage()).into(holder.recipeImage);
            } else {
                Toast.makeText(holder.recipeImage.getContext(), "No network available", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void recipesHaveUpdated(List<Result> recipes) {
        this.recipes = recipes;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (recipes == null) {
            return 0;
        } else {
            return recipes.size();
        }
    }

    public class RecipeListAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.recipe_image)
        ImageView recipeImage;
        @BindView(R.id.recipe_list_item_name)
        TextView recipeListItemName;
        @BindView(R.id.recipe_list_item_serving_placeholder)
        TextView recipeListItemServingPlaceholder;

        public RecipeListAdapterViewHolder(View itemView) {
            super(itemView);

            // Bind the fields
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int clickedPosition = getAdapterPosition();
            listener.recipeListOnClick(recipes.get(clickedPosition));
            Log.d("The position clicked is", String.valueOf(clickedPosition));
        }
    }
}
