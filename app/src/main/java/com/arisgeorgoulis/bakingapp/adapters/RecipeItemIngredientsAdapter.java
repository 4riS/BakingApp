package com.arisgeorgoulis.bakingapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arisgeorgoulis.bakingapp.R;
import com.arisgeorgoulis.bakingapp.data.Ingredient;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeItemIngredientsAdapter extends RecyclerView.Adapter<RecipeItemIngredientsAdapter.RecipeItemIngredientsAdapterViewHolder> {

    private List<Ingredient> ingredients;

    public RecipeItemIngredientsAdapter(List<Ingredient> ingredients){
        this.ingredients = ingredients;
    }

    @NonNull
    @Override
    public RecipeItemIngredientsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForRecyclerItem = R.layout.recipe_item_ingredient;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;
        View view = inflater.inflate(layoutIdForRecyclerItem, parent, shouldAttachToParentImmediately);
        return new RecipeItemIngredientsAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeItemIngredientsAdapterViewHolder holder, int position) {
        Ingredient ingredient = ingredients.get(position);
        holder.recipeIngredientQuantity.setText(String.valueOf(ingredient.getQuantity()));
        holder.recipeIngredientMeasurement.setText(ingredient.getMeasure());
        holder.recipeIngredientIngredient.setText(ingredient.getIngredient());
    }

    @Override
    public int getItemCount() {
        if (ingredients == null) {
            return 0;
        } else {
            return ingredients.size();
        }
    }

    public class RecipeItemIngredientsAdapterViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.recipe_ingredients_quantity_placeholder)
        TextView recipeIngredientQuantity;
        @BindView(R.id.recipe_ingredients_measurement_placeholder)
        TextView recipeIngredientMeasurement;
        @BindView(R.id.recipe_ingredients_ingredient_placeholder)
        TextView recipeIngredientIngredient;

        public RecipeItemIngredientsAdapterViewHolder(View itemView) {
            super(itemView);

            // Bind the fields
            ButterKnife.bind(this, itemView);
        }
    }
}
