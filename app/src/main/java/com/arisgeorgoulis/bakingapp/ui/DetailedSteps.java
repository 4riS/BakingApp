package com.arisgeorgoulis.bakingapp.ui;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.arisgeorgoulis.bakingapp.R;
import com.arisgeorgoulis.bakingapp.data.Step;
import com.arisgeorgoulis.bakingapp.fragments.RecipeDetailFragment;

public class DetailedSteps extends AppCompatActivity {

    private Step step;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_steps);

        if (savedInstanceState == null) {
            step = getIntent().getParcelableExtra(RecipeItem.STEP);

            // Create the fragment
            RecipeDetailFragment detailFragment = new RecipeDetailFragment();
            detailFragment.setStep(this.step);

            // Fragment manager initialize
            FragmentManager fragmentManager = getSupportFragmentManager();

            fragmentManager.beginTransaction()
                    .add(R.id.detail_step, detailFragment)
                    .commit();
        }
    }
}
