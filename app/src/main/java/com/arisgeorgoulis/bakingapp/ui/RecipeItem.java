package com.arisgeorgoulis.bakingapp.ui;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.arisgeorgoulis.bakingapp.MainActivity;
import com.arisgeorgoulis.bakingapp.R;
import com.arisgeorgoulis.bakingapp.adapters.RecipeItemStepsAdapter;
import com.arisgeorgoulis.bakingapp.data.Ingredient;
import com.arisgeorgoulis.bakingapp.data.Step;
import com.arisgeorgoulis.bakingapp.fragments.RecipeDetailFragment;
import com.arisgeorgoulis.bakingapp.fragments.RecipeItemIngredientsFragment;
import com.arisgeorgoulis.bakingapp.fragments.RecipeItemStepsFragment;
import com.arisgeorgoulis.bakingapp.utilities.TabletUtils;

import java.util.List;

public class RecipeItem extends AppCompatActivity {
    public final static String STEP = "Step";


    private List<Ingredient> ingredients;
    private List<Step> steps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_item);

        if (savedInstanceState == null) {
            ingredients = getIntent().getParcelableArrayListExtra(MainActivity.RECIPE_INGREDIENTS);
            RecipeItemIngredientsFragment ingredientsFragment = new RecipeItemIngredientsFragment();
            ingredientsFragment.setIngredients(this.ingredients);

            // Fragment manager initialize
            FragmentManager fragmentManager = getSupportFragmentManager();

            fragmentManager.beginTransaction()
                    .add(R.id.recipe_item_ingredients, ingredientsFragment)
                    .commit();

            steps = getIntent().getParcelableArrayListExtra(MainActivity.RECIPE_STEPS);
            RecipeItemStepsFragment stepsFragment = new RecipeItemStepsFragment();
            stepsFragment.setSteps(this.steps);

            fragmentManager.beginTransaction()
                    .add(R.id.recipe_item_steps, stepsFragment)
                    .commit();
        }

        if (findViewById(R.id.tablet_item_layout) != null){
            TabletUtils.IS_TABLET = true;
            if (savedInstanceState == null){
                RecipeDetailFragment detailFragment = new RecipeDetailFragment();
                detailFragment.setStep(steps.get(0));

                // Fragment manager initialize
                FragmentManager fragmentManager = getSupportFragmentManager();

                fragmentManager.beginTransaction()
                        .add(R.id.recipe_item_details, detailFragment)
                        .commit();
            }
        } else {
            TabletUtils.IS_TABLET = false;
        }
    }
}
