package com.arisgeorgoulis.bakingapp.widget;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RemoteViews;

import com.arisgeorgoulis.bakingapp.APIClients.RecipeAPIClient;
import com.arisgeorgoulis.bakingapp.R;
import com.arisgeorgoulis.bakingapp.adapters.RecipeListAdapter;
import com.arisgeorgoulis.bakingapp.data.Ingredient;
import com.arisgeorgoulis.bakingapp.data.Result;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WidgetListActivity extends AppCompatActivity implements RecipeListAdapter.RecipeAdapterOnClickHandler, RecipeAPIClient.updateTheAdapterListener<List<Result>> {
    // Parcelable constants
    private final static String PARC_RECIPES = "RecipesParcelable";
    private final static String PARC_LIST_POSITION = "RecipesPositionOnList";

    @BindView(R.id.recipe_list)
    RecyclerView recipeList;
    private RecipeListAdapter recipeListAdapter;
    private LinearLayoutManager recipeListLayoutManager;
    private List<Result> recipes = null;

    AppWidgetManager appWidgetManager;
    int mAppWidgetId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget_list);

        // Bind the fields
        ButterKnife.bind(this);


        RecipeAPIClient recipeAPICLient = new RecipeAPIClient(this);
        recipeAPICLient.getTheRecipes();
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recipeList.setHasFixedSize(false);

        // Create the layout for recipe list
        recipeListLayoutManager = new LinearLayoutManager(this);
        recipeList.setLayoutManager(recipeListLayoutManager);

        if (savedInstanceState != null) {
            this.recipes = savedInstanceState.getParcelableArrayList(PARC_RECIPES);
        }

        // Create the adapter
        recipeListAdapter = new RecipeListAdapter(this.recipes, this);

        // Set the adapter on the GridView
        recipeList.setAdapter(recipeListAdapter);

        if (savedInstanceState != null) {
            int mCurrentPosition = savedInstanceState.getInt(PARC_LIST_POSITION);
            if (mCurrentPosition != -1){
                recipeList.smoothScrollToPosition(mCurrentPosition);
            }
        }

        appWidgetManager = AppWidgetManager.getInstance(this);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If they gave us an intent without the widget id, just bail.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(PARC_RECIPES, (ArrayList<? extends Parcelable>) this.recipes);
        outState.putInt(PARC_LIST_POSITION, recipeListLayoutManager.findFirstVisibleItemPosition());
    }

    @Override
    public void recipeListOnClick(Result recipe) {
        RemoteViews views = new RemoteViews(this.getPackageName(),
                R.layout.baking_app_widget);
        views.setTextViewText(R.id.appwidget_heading, recipe.getName());
        StringBuilder ingredientsFull = new StringBuilder();
        for (Ingredient ingredient:recipe.getIngredients()) {
            ingredientsFull.append("- ")
                    .append(ingredient.getIngredient())
                    .append(" ")
                    .append(ingredient.getQuantity())
                    .append(" ")
                    .append(ingredient.getMeasure())
                    .append("\n");
        }
        views.setTextViewText(R.id.appwidget_placeholder, ingredientsFull);
        appWidgetManager.updateAppWidget(mAppWidgetId, views);
        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        setResult(RESULT_OK, resultValue);
        finish();
    }

    @Override
    public void updateTheAdapter(List<Result> recipes) {
        this.recipes = recipes;
        recipeListAdapter.recipesHaveUpdated(this.recipes);
    }
}
