package com.arisgeorgoulis.bakingapp;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;

import com.arisgeorgoulis.bakingapp.adapters.RecipeItemStepsAdapter;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToHolder;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class StepsAreShownCorrectlyWithTextTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void stepsHaveTheCorrectText(){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.recipe_list)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.recipe_steps)).perform(RecyclerViewActions
                .actionOnItem((hasDescendant(withText("0.Recipe Introduction"))), click()));
    }

    public static Matcher<RecyclerView.ViewHolder> withTitle(final String title)
    {
        return new BoundedMatcher<RecyclerView.ViewHolder, RecipeItemStepsAdapter.RecipeItemStepsAdapterViewHolder>(RecipeItemStepsAdapter.RecipeItemStepsAdapterViewHolder.class)
        {
            @Override
            protected boolean matchesSafely(RecipeItemStepsAdapter.RecipeItemStepsAdapterViewHolder item)
            {
                return item.itemView.getContext().getText(R.id.recipe_steps_placeholder).toString().equalsIgnoreCase(title);
            }

            @Override
            public void describeTo(Description description)
            {
                description.appendText("view holder with title: " + title);
            }
        };
    }
}
